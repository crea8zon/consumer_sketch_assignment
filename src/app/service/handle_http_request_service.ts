import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {DataModel} from "../models/DataModel";
import {ParentData} from "../models/ParentData";


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient) {
  }

  public getData(pageNumber: Number): Observable<ParentData> {
    return this.http.get<ParentData>(environment.serverUrl + '?' + 'page=' + pageNumber + '&size=' + 10 );
  }
}
