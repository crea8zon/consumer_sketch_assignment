import {Component, OnInit} from '@angular/core';
import {HttpService} from "../service/handle_http_request_service";
import {ParentData} from "../models/ParentData";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  data: ParentData | undefined;
  pageNumber = 0;
  totalPassengers = 0;
  isLoading = true;

  constructor(private service: HttpService) {
  }

  ngOnInit(): void {
    this.pageNumber = 1;
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.service.getData(this.pageNumber).subscribe((result) => {
      this.isLoading = false;
      this.data = result;
      if (!!result.totalPassengers) {
        this.totalPassengers = result.totalPassengers/10;
      }
    }, error => {

    })
  }

  loadNextData(decider: string) {
    if (decider === 'first') {
      this.pageNumber = 1;
    } else if (decider === 'prev') {
      this.pageNumber--;
    } else if (decider === 'next') {
      this.pageNumber++;
    } else if (decider === 'last'){
      this.pageNumber = this.totalPassengers;
    }
    this.getData()
  }

}
