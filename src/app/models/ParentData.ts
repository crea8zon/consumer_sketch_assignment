import {DataModel} from "./DataModel";

export class ParentData {
  data?: DataModel[]
  totalPassengers?: number;
}
